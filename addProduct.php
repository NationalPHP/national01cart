<?php
include "functions.php";
?>
<html lang="en">
<?php include "parts/head.php"; ?>
    <body>
        <div class="container">
            <?php include "parts/header.php"; ?>
            <div class="row">
                <form method="post" action="processAddProduct.php" enctype="multipart/form-data">
                    <div class="col-12 form-group">
                        <label for="formName">Name:</label>
                        <div class="row">
                            <div class="col-8">
                                <input class="form-control" id="formName" type="text" placeholder="Name" name="name" />
                            </div>
                            <div class="col-4">
                                <button
                                    ondblclick="document.getElementById('formName').style.borderColor='#ff0000';"
                                    onclick="document.getElementById('formName').value = 'TEST123';"
                                    type="button" class="btn btn-danger">X</button>
                            </div>
                    </div>
                    <div class="col-12 form-group">
                        <label for="formPrice">Price:</label>
                        <input onblur="if (parseInt(this.value) < 0) { alert('Nu ai voie cu pret NEGATIV!!!'); }" class="form-control" id="formPrice" type="number" placeholder="Price" name="price" />
                    </div>
                    <div class="col-12 form-group">
                        <label for="formCategory">Category:</label>
                        <select class="form-control" id="formCategory" name="category_id">
                            <?php foreach (Category::findAll() as $category): ?>
                                <option value="<?php echo $category->getId() ?>"><?php echo $category->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-12 form-group">
                        <label for="formDiscount">Discount:</label>
                        <input class="form-control" id="formDiscount" type="number" placeholder="Discount" name="discount" />
                    </div>
                    <div class="col-12 form-group">
                        <label for="formCode">Code:</label>
                        <input class="form-control" id="formCode" type="text" placeholder="Code" name="code" />
                    </div>
                    <div class="col-12 form-group">
                        <label for="formReview">Review number:</label>
                        <input class="form-control" id="formReview" type="number" placeholder="Review number" name="review_number" />
                    </div>
                    <div class="col-12 form-group">
                        <label for="formPercent">Percent:</label>
                        <input class="form-control" id="formPercent" type="number" placeholder="Percent" name="percent" />
                    </div>
                    <div class="col-12 form-group">
                        <label for="formImage">Image:</label>
                        <input class="form-control" id="formImage" type="file" placeholder="Image" name="image" />
                    </div>
                    <div class="col-12 form-group">
                        <label for="formServices">Servicii:</label>
                        <select multiple class="form-control" id="formServices" name="service[]">
                            <?php foreach (Service::findBy('category_id',4) as $service): ?>
                                <option value="<?php echo $service->getId() ?>"><?php echo $service->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-12 form-group">
                        <button class="btn btn-primary" type="submit" >Save</button>
                        <button class="btn btn-primary" type="reset" >Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>