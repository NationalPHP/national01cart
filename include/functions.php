<?php
include "config.php";
session_start();
$redis = new Redis();
$redis->connect($redisHost, $redisPort);
if (!$redis->get('first')){
    $redis->set("first", "VGVzdCBmaW5hbGl6YXQhISE=");
}
$conn = mysqli_connect($MySQLHost,$MySQLUsername,$MySQLPassword,$MySQLDatabase);

function runQuery($sql){
    global $conn;
    $query = mysqli_query($conn, $sql);

    if (!$query){
        die("Mysql error on query:$sql - ".mysqli_error($conn));
    }
    if (is_bool($query)){
        return mysqli_insert_id($conn);
    } else {
        return $query->fetch_all(MYSQLI_ASSOC);
    }
}

