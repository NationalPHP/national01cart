<?php
//REDIS config
$redisHost = '127.0.0.1';
$redisPort = 6379;

//NySQL config
$MySQLHost = '127.0.0.1';
$MySQLUsername = 'root';
$MySQLPassword = 'pass';
$MySQLDatabase = 'devOpsTest';