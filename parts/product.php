<div class="card" style="width: 18rem;">
    <img class="card-img-top" src="images/<?php echo $product->image; ?>" alt="<?php echo $product->name; ?>">
    <div class="card-body">
        <h5 class="card-title"><?php echo $product->name; ?></h5>
        <p class="card-text">Brand: <a href="brand.php?id=<?php echo $product->brand_id; ?>"> <?php echo $product->getBrand()->name; ?></a></p>
        <p class="card-text">Age: <?php echo $product->age; ?></p>
        <p class="card-text"><?php echo $product->price; ?> RON</p>

        <button data-url="addToCart.php?id=<?php echo $product->getId(); ?>" class="btn btn-primary myAddToCart">Adauga in cos</button>
    </div>
</div>