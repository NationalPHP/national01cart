<div class="row">
    <div class="col-12">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="index.php">Home</a>
            </li>
            <?php foreach (Category::findBy('parent_id',0) as $mainCategory): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="category.php?id=<?php echo $mainCategory->getId(); ?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <?php echo $mainCategory->name; ?>
                </a>
                <div class="dropdown-menu">
                    <?php foreach ($mainCategory->getChildren() as $child): ?>
                        <a class="dropdown-item" href="category.php?id=<?php echo $child->getId(); ?>"><?php echo $child->name; ?></a>
                    <?php endforeach; ?>
                </div>
            </li>
            <?php endforeach; ?>
            <li class="nav-item">
                <a class="nav-link active" href="cart.php">Cart <spam id="myCartTotalProduct" class="badge badge-danger"><?php echo getCurrentCart()->getTotalProducts(); ?></spam></a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <?php if (isset($_SESSION['success_message'])): ?>
            <div class="alert alert-success" role="alert">
                <?php
                    echo $_SESSION['success_message'];
                    unset($_SESSION['success_message']);
                ?>
            </div>
        <?php endif; ?>
        <?php if (isset($_SESSION['error_message'])): ?>
            <div class="alert alert-danger" role="alert">
                <?php
                echo $_SESSION['error_message'];
                unset($_SESSION['error_message']);
                ?>
            </div>
        <?php endif; ?>
    </div>
</div>
