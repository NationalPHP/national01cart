<?php
include "functions.php";
$brand = Brand::find($_GET['id']);
?>
<html lang="en">
<?php include "parts/head.php"; ?>
<body>
<div class="container">
    <?php include "parts/header.php"; ?>
    <div class="row">
        <?php foreach ($brand->getProducts() as $product): ?>
            <div class="col-3">
                <?php include "parts/product.php"; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</body>
</html>