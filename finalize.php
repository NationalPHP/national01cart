<?php
include "functions.php";
$cart = getCurrentCart();
require __DIR__ . '/vendor/autoload.php';
$pdf = new TCPDF('L');                 // create TCPDF object with default constructor args
$pdf->SetMargins(20, 20, 20, true);
$pdf->AddPage();

ob_start();
?>
<table>
    <tr>
        <td>Date vanzator</td>
        <td>
            <h1>FACTURA FISCALA</h1>
            <h2>Nr:<?php echo $cart->getId().'/'.date('Y-m-d'); ?></h2>
        </td>
        <td>Date cumparator</td>
    </tr>
</table>
<table border="1">
    <tr>
        <th><b>Product</b></th>
        <th><b>Quantity</b></th>
        <th>Price</th>
        <th>VAT</th>
        <th>Total</th>
    </tr>
    <?php foreach ($cart->getCartProducts() as $cartProduct): ?>
        <tr class="line">
            <td><?php echo $cartProduct->getProduct()->name; ?></td>
            <td>
                <?php echo $cartProduct->quantity; ?>
            </td>
            <td><?php echo $cartProduct->getProduct()->price; ?> RON</td>
            <td><?php echo round($cartProduct->getProduct()->price*0.19,2); ?> RON</td>
            <td><?php echo $cartProduct->getTotal(); ?> RON</td>

        </tr>
    <?php endforeach; ?>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <th>TOTAL:</th>
        <th><?php echo $cart->getTotal(); ?> RON</th>
    </tr>
</table>

<?php
$html = ob_get_clean();
$pdf->writeHTML($html);     // 1 is line height

$pdf->Output('/var/www/html/national01/daniel/national01cart/invoices/cart.pdf','F');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

$mail = new PHPMailer(true);


    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'smtp.webfaction.com';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'mailsendvps';                     //SMTP username
    $mail->Password   = 'secret';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('daniel@ianosi.ro', 'Daniel Ianosi');
    $mail->addAddress('ianosid@yahoo.com', 'Ianosi Daniel');     //Add a recipient
    $mail->addReplyTo('orders@ianosi.ro', 'Orders');

    //Attachments
    $mail->addAttachment('/var/www/html/national01/daniel/national01cart/invoices/cart.pdf', 'invoice.pdf');    //Optional name

    //Content
    $mail->isHTML(true);                                  //Set email format to HTML
    $mail->Subject = 'Comanda finalizata';
    $mail->Body    = $html;
    $mail->AltBody = 'Multumim pentru comanda plasata!';

    $mail->send();

?>
