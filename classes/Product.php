<?php


class Product extends BaseClass
{
    public $name;

    public $description;

    public $price;

    public $age;

    public $category_id;

    public $brand_id;

    public $image;

    public function getBrand()
    {
        return Brand::find($this->brand_id);
    }

    public static function getTableName()
    {
        return 'products';
    }

}
