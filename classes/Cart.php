<?php


class Cart extends BaseClass
{
    public $address;

    public function getCartProducts()
    {
        return CartProduct::findBy('cart_id', $this->getId());
    }

    public function toJSON()
    {
        $data =[
            'cartTotal' => $this->getTotal(),
            'cartTotalProducts' => $this->getTotalProducts()
        ];

        return json_encode($data);
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->getCartProducts() as $cartProduct){
            $total+= $cartProduct->getTotal();
        }

        return round($total,2);
    }

    public static function getTableName()
    {
        return 'carts';
    }

    public function getTotalProducts()
    {
        $total = 0;
        foreach ($this->getCartProducts() as $cartProduct){
            $total+=$cartProduct->quantity;
        }

        return $total;
    }

    public function add(Product $product)
    {
        foreach ($this->getCartProducts() as $cartProduct){
            if ($product->getId() == $cartProduct->product_id){
                $cartProduct->quantity+=1;
                $cartProduct->save();
                return;
            }
        }

        $cartProduct = new CartProduct();
        $cartProduct->quantity = 1;
        $cartProduct->product_id = $product->getId();
        $cartProduct->cart_id = $this->getId();
        $cartProduct->save();
    }
}