<?php


class CartProduct extends BaseClass
{
    public $product_id;

    public $quantity;

    public $cart_id;

    public function getProduct()
    {
        return Product::find($this->product_id);
    }

    public function getCart()
    {
        return Cart::find($this->cart_id);
    }

    public function toJSON()
    {
        $data =[
            'id' => $this->getId(),
            'lineTotal' => $this->getTotal(),
            'cartTotal' => $this->getCart()->getTotal(),
            'cartTotalProducts' => $this->getCart()->getTotalProducts()
        ];

        return json_encode($data);
    }

    public function getTotal()
    {
        if ($this->quantity > 10){
            return round($this->quantity * $this->getProduct()->price *0.9, 2);
        } else {
            return round($this->quantity * $this->getProduct()->price,2);
        }
    }

    public static function getTableName()
    {
        return 'cart_products';
    }
}