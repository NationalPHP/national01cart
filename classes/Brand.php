<?php

class Brand extends BaseClass
{
    public $name;

    public function getProducts()
    {
        return Product::findBy('brand_id', $this->getId());
    }

    public static function getTableName()
    {
        return 'brands';
    }
}