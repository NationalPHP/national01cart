<?php


class Service extends Product
{
   public $percent;

    public function getPrice($parent=null)
    {
        return $parent->getPrice() * $this->percent / 100;
    }

    /**
    * @return mixed
    */
    public function getName($parent=null)
    {
        $parentName = $parent->getName();
        return $this->name."($parentName)";
    }


}