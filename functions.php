<?php
spl_autoload_register(function ($class_name) {
    include "classes/".$class_name . '.php';
});


$salt = 'Mys3cr3ts@lt';
session_start();
$conn = mysqli_connect('127.0.0.1','root','Sco@l@it123','national-01-daniel-cart');

function runQuery($sql){
    global $conn;
    $query = mysqli_query($conn, $sql);

    if (!$query){
        die("Mysql error on query:$sql - ".mysqli_error($conn));
    }
    if (is_bool($query)){
        return mysqli_insert_id($conn);
    } else {
        return $query->fetch_all(MYSQLI_ASSOC);
    }
}


function checkLogin(){
    if (!isset($_SESSION['user_id'])){
        header('Location: login.php');
        die;
    }
}

function getCurrentCart()
{
    //data exista in session cart_id
    if (isset($_SESSION['cart_id'])){
        return Cart::find($_SESSION['cart_id']);
    } else {
        $cart = new Cart();
        $cart->save();
        $_SESSION['cart_id'] = $cart->getId();
        return $cart;
    }
}


