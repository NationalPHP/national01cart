<?php
include "functions.php";

var_dump($_POST);
//var_dump($_FILES);


$data = $_POST;
unset($data['service']);

$product = new Product();
$product->fromArray($data);
$product->image = $_FILES['image']['name'];
$product->save();

$services = $_POST['service'];
foreach ($services as $serviceId){
    $productService = new ProductService();
    $productService->service_id = $serviceId;
    $productService->product_id = $product->getId();
    $productService->save();
}

$imageName = $product->getId().'.jpg';
move_uploaded_file($_FILES['image']['tmp_name'], 'images/'.$imageName);
$product->image = $imageName;
$product->save();

$_SESSION['success_message'] = 'Produs adaugat cu succes';
header('Location: index.php');
