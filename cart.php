<?php
include "functions.php";
$cart = getCurrentCart();
?>
<html lang="en">
<?php include "parts/head.php"; ?>
<body>

<div class="container">
    <?php include "parts/header.php"; ?>
    <div class="row">
        <table id="myTable" class="table table-bordered">
            <tr>
                <th>Image</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
                <th>Delete</th>
            </tr>
            <?php foreach ($cart->getCartProducts() as $cartProduct): ?>
                <tr class="line">
                    <td>
                        <img src="images/<?php echo $cartProduct->getProduct()->image; ?>" height="70" />
                    </td>
                    <td><?php echo $cartProduct->getProduct()->name; ?></td>
                    <td>
                        <button onclick="changeValue('quantity<?php echo $cartProduct->getId(); ?>','-')" class="btn btn-primary" type="button">-</button>
                        <input data-url="updateCart.php?id=<?php echo $cartProduct->getId(); ?>" class="cartProductQuantity" id="quantity<?php echo $cartProduct->getId(); ?>" type="text" value="<?php echo $cartProduct->quantity; ?>" />
                        <button onclick="changeValue('quantity<?php echo $cartProduct->getId(); ?>','+')" class="btn btn-primary" type="button">+</button>

                    </td>
                    <td><?php echo $cartProduct->getProduct()->price; ?> RON</td>
                    <td><span id="lineTotal<?php echo $cartProduct->getId(); ?>"><?php echo $cartProduct->getTotal(); ?></span> RON</td>
                    <td><button id="quantity<?php echo $cartProduct->getId(); ?>Delete" data-url="removeFromCart.php?id=<?php echo $cartProduct->getId(); ?>" class="btn btn-danger myDeleteButton" type="button">X</button></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <th>TOTAL:</th>
                <th><span id="myCartTotal"><?php echo $cart->getTotal(); ?></span> RON</th>
                <th><a href="finalize.php" class="btn btn-danger">Finalizeaza</a> </th>
            </tr>
        </table>

        <h3>Address:<?php echo $cart->address; ?></h3>
        <div id="log">
        </div>
    </div>
</div>
<script>
    function changeValue(inputId, operation){
        if (operation == '+'){

            if ($('#'+inputId).val() <10 ){
                $('#'+inputId).val(parseInt($('#'+inputId).val()) + 1);
                $('#'+inputId).change();
            }
        } else {
            if ($('#'+inputId).val() > 1){
                $('#'+inputId).val(parseInt($('#'+inputId).val()) - 1);
                $('#'+inputId).change();
            } else {
                $('#'+inputId+'Delete').click();
            }
        }
        $('#log').append('<div class="alert alert-success" role="alert">Am schimbat idul:'+inputId+' cu operatiunea '+operation+'!!!</div>');
    }

    $('.line').each(function (index){
        if (index % 2 ==1){
            $(this).css('background-color','#ccc');
        }
    });

    $('.line').hover(function (){
        $(this).css('background-color','green');
    });

    $('.line').mouseleave(function (){
        if ($('.line').index(this) % 2 ==1){
            $(this).css('background-color','#ccc');
        } else {
            $(this).css('background-color','#fff');
        }
    });

</script>
</body>
</html>