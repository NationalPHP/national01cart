<?php
include "functions.php";
$category = Category::find($_GET['id']);
?>
<html lang="en">
<?php include "parts/head.php"; ?>
<body>
<div class="container">
    <?php include "parts/header.php"; ?>
    <div class="row">
        <?php foreach ($category->getProducts() as $product): ?>
            <div class="col-3">
                <?php include "parts/product.php"; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</body>
</html>