<?php
    include "functions.php";
?>
<html lang="en">
<?php include "parts/head.php"; ?>
    <body>
        <div class="container">
            <?php include "parts/header.php"; ?>
            <div class="row">
                <?php foreach (Product::findAll() as $product): ?>
                    <div class="col-3">
                        <?php include "parts/product.php"; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </body>
</html>